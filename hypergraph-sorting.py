import math

TEXT_DATASET_LOCATION = "hypergraph.csv"
TEXT_OUTPUT_LOCATION = "output.csv"


#Parse the hypergraph in a .csv file to a list

def parseHypergraph(file_location):

    localHyperlayer = []
    i = 0
    
    with open(file_location, encoding="utf8") as file:
        
        for line in file:
            if i == 0:
                i = i+1
                pass
            else:
                leftHand = []
                rightHand = []
                weight = 0
                splitString = line.split(",")
                leftHand = splitString[0].split(" AND ")
                rightHand = splitString[1].split(" AND ")
                    
                if len(splitString) == 3:
                    weight = splitString[2]
                    localHyperlayer.append([leftHand, rightHand, int(weight)])
                else:
                    localHyperlayer.append([leftHand, rightHand])
            
        file.close()

    return localHyperlayer


#Extract the first line header
def extractBuffer(file_location):
    with open(file_location, encoding="utf8") as file:
        for line in file:
            buffer = line
            file.close()
            return buffer
            break


#Write to a .csv file, ordering the edges by adjacency relationships
def writeHypergraph(file_location, localHyperlayer):

    file2 = open(file_location, "a")
    written = []
    
    if len(localHyperlayer) > 1:

        for i in range(len(localHyperlayer)):
            for i2 in range(len(localHyperlayer)):
                if localHyperlayer[i][1] == localHyperlayer[i2][0]:
                    if (localHyperlayer[i] not in written):
                        
                        file2.write(str.join(" AND ", localHyperlayer[i][0]) + "," + str.join(" AND ", localHyperlayer[i][1]) + "," + str(localHyperlayer[i][2]) + "\n")
                        written.append(localHyperlayer[i])
                        
                    file2.write(str.join(" AND ", localHyperlayer[i2][0]) + "," + str.join(" AND ", localHyperlayer[i2][1]) + "," + str(localHyperlayer[i2][2]) + "\n")
                    written.append(localHyperlayer[i2])
    else:
        file2.write(str.join(" AND ", localHyperlayer[0][0]) + "," + str.join(" AND ", localHyperlayer[0][1]) + "," + str(localHyperlayer[0][2]) + "\n")


    for v in localHyperlayer:
        if v not in written:
            file2.write(str.join(" AND ", v[0]) + "," + str.join(" AND ", v[1]) + "," + str(v[2]) + "\n")


    file2.close()


def main():

    #Write first line headers to a .csv file
    file = open(TEXT_OUTPUT_LOCATION, "a")
    file.write(extractBuffer(TEXT_DATASET_LOCATION))
    file.close()
    
    print("Hypergraph:\n " + str(parseHypergraph(TEXT_DATASET_LOCATION)))
    writeHypergraph(TEXT_OUTPUT_LOCATION,parseHypergraph(TEXT_DATASET_LOCATION))
    print("\nOutput complete.")


if __name__ == "__main__":
    main()
