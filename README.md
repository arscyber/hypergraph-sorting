# README #

This script contains several functions and algorithms. These functions allow to transform CSV files with a specific formatting to hypergraphs, and to sort hypergraphs based on their adjacency.
This script is in its early alpha stage.
The goal is to develop a multilayer hypergraph data type that can be parsed from separate hypergraph layer files.

Once complete, the applications could range from Supply Chain Visualization and calculation, to text generation through parallel Breadth-first algorithms combined with real-time shortest path calculations, to predicting and stopping the spread of a biological virus.

As of now, combining such data types with search and path calculating algorithm could allow for estimating profitable supply chains, for example, since the edge weights could represent the value added, and path lengths could be calculated.

We are planning to outsource software development from now on, as well as to collect more funding.


### hypergraph.csv ###
The hypergraph.csv file contains an example of the formatting used.


### Additional information ###

* [Hypergraphs on Wikipedia](https://en.wikipedia.org/wiki/Hypergraph)